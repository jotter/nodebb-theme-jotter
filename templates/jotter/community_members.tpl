<div class="_people people">
	<div class="everything">
	<div class="everything-people _gpu">	
	<div class="container no-p-lr">
	<section>
			<div class="col-md-10 col-md-offset-1 no-p-lr">
				<div class="people-wrapper">
					<div class="users-container clearfix">
						<h3 class="content-heading">Members of {category.name} community</h3>
						<div id="members-container" data-nextstart="{nextStart}">
							<!-- IMPORT partials/members_list.tpl -->
						</div>	
					</div>
				</div>
			</div>
	</section>
	</div>
	</div>
	</div>
</div>

<!-- DO NOT REMOVE BELOW -->
<input type="hidden" template-variable="nextStart" value="{nextStart}" />
<input type="hidden" template-variable="communitySlug" value="{category.slug}" />
<input type="hidden" template-variable="communityId" value="{category.cid}" />

<script>
	'use strict';
	require(['forum/people','forum/jinfinitescroll'], function(people,scroll) {
    $('.contextual--search-input').attr('placeholder', 'Start typing to find interesting people...');
    people.init();
		scroll.init('partials/members_list');
	});
</script>
