<div class="_communities communities">

<!-- IF !loggedIn -->
<div id="not-loggedin" class="not-loggedin not-loggedin-communities">
	<div class="not-loggedin-wrapper">
	<div class="not-loggedin-welcome">Welcome to Jotter</div>
	<h3 class="not-loggedin-heading">Sign up to join conversations that mean something.</h3>
		<div class="not-loggedin-intent">
			<a class="not-loggedin-btn" 
			   href="/auth/twitter" 
			   title="Join with Twitter" target="_top">
				<i class="fa fa-twitter"></i>
				<span class="not-loggedin-text">Twitter</span>
			</a>
			<a class="not-loggedin-btn" 
			   href="/auth/facebook" 
			   title="Join with Facebook" target="_top">
				<i class="fa fa-facebook-official"></i>
				<span class="not-loggedin-text">Facebook</span>
			</a>
			<a class="not-loggedin-btn" 
			   href="/auth/google" 
			   title="Join with Google+" target="_top">
				<i class="fa fa-google-plus"></i>
				<span class="not-loggedin-text">Google+</span>
			</a>
		</div>
		<div class="not-loggedin-footer">&copy; 2015 Jotter, Inc.
			<a class="url" href="mailto:info@jotter.com">Contact</a>
		</div>
	</div>
</div>
<div class="everything everything-faux">
<div class="everything-communities _gpu">	
<div class="container no-p-lr">
<section>
		<div class="col-md-10 col-md-offset-1 no-p-lr">
						
						
			<header class="u-offset">
				<div class="content-navigation">
				<a href="#" class="content-navigation-item url active">---</a>
				<a href="#" class="content-navigation-item url">--</a>
				</div>
				<h3 class="content-navigation-text">-- - -- --</h3>
			</header>
			
			<div class="communities-wrapper">
				<!-- // COMMUNITIES (FAUX) --> 
				<div class="communities-container clearfix">
					<!-- IMPORT partials/communities/faux.tpl -->
				</div>
			</div>
		</div>
</section>
</div>
</div>
</div>
<!-- ELSE -->

<div class="everything">
<div class="everything-communities _gpu">	
<div class="container no-p-lr">
<section>
		<div class="col-md-10 col-md-offset-1 no-p-lr">
			
			
			<header class="u-offset">
				<div class="content-navigation">
					<a href="/communities" class="content-navigation-item url active">Communities</a>
					<a href="/people" class="content-navigation-item url">People</a>
				</div> <br>
				<h3 class="content-navigation-text hide">Pick and choose what you're interested in</h3>
			</header>

			<!-- IMPORT partials/contextual_search.tpl -->
			
			<div class="communities-wrapper">
				<!-- // COMMUNITIES -->
				<!-- IF joinedCategories.length -->
				<div class="communities-container clearfix" data-nextstart="{nextStart}">
					<!-- IMPORT partials/communities/joined_card.tpl -->
				</div>
				<!-- ENDIF joinedCategories.length -->
				<div  class="communities-container clearfix" data-nextstart="{nextStart}">
					<div class="content_separator"></div>
					<div id="communities-container">
					<!-- IMPORT partials/communities/card.tpl -->
					</div>
				</div>
			</div>
		</div>
</section>
</div>
</div>
</div>
<!-- ENDIF !loggedIn -->


</div>


<!-- DO NOT REMOVE BELOW -->
<input type="hidden" template-variable="nextStart" value="{nextStart}" />
<script>
	'use strict';
	require(['forum/jinfinitescroll'], function(scroll) {
    $('.contextual--search-input').attr('placeholder', 'Start typing to discover interesting communities...');
		scroll.init('partials/communities/card');
	});
</script>