<div class="_people people">
	<div class="everything">
		<div class="everything-people _gpu">	
			<div class="container no-p-lr">
				<section>
					<div class="col-md-10 col-md-offset-1 no-p-lr">
						
						<header class="u-offset">
							<div class="content-navigation">
								<a href="/communities" class="content-navigation-item url">Communities</a>
								<a href="/people" class="content-navigation-item url active">People</a>
							</div> <br>
							<h3 class="content-navigation-text hide">Search for people who share your interests
								
								<!-- @todo: functionality
									 These anchor links should scroll to the
									 appropriate content the user is looking for.
								-->
								<a class="content-navigation-jump hidden-xs">Following</a>
								<a class="content-navigation-jump hidden-xs">Everyone</a>
							</h3>
						</header>

						<!-- IMPORT partials/contextual_search.tpl -->
						
						<div class="people-wrapper">
							<!-- IF following.length -->
							<div  id="people-following-container"  class="users-container clearfix" data-nextstart="{nextStart}"
										data-toggle="collapse" data-target="#collapse_people_list">
								<div id="collapse_people_list" class="collapse in">
									<!-- IMPORT partials/users_list_following.tpl -->
								</div>	
							</div>
							
							<!-- ENDIF following.length -->
							
							<!-- // PEOPLE EVERYONE -->
							<div class="users-container clearfix">
								<div class="content_separator"></div>
								<div id="people-container" data-nextstart="{nextStart}">
									<!-- IMPORT partials/users_list.tpl -->
								</div>	
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>

<!-- DO NOT REMOVE BELOW -->
<input type="hidden" template-variable="nextStart" value="{nextStart}" />

<script>
	'use strict';
	require(['forum/people','forum/jinfinitescroll'], function(people,scroll) {
    $('.contextual--search-input').attr('placeholder', 'Start typing to find interesting people...');
    people.init();
		scroll.init('partials/users_list');
	});
</script>