<div class="topic">
	<div class="everything">
		<div class="everything-topic _gpu">
			
			<!-- //TOPIC CONTAINER -->
			<div class="everything-container">
				<div class="container">

					<div class="col-md-10 col-md-offset-1 no-p-lr">
					
						<div class="topic-header col-md-8 col-md-offset-2">
							<a class="topic-backto" href="{config.relative_path}/category/{category.slug}">{category.name}</a>
						</div>
						
						<div class="topic-wrapper<!-- IF showRead --> topic-link-submission<!-- ELSE --> topic-text-submission<!-- ENDIF showRead --> col-md-8 col-md-offset-2">
							
							<!-- //TOPIC ACTION BUTTONS -->
							<div class="topic-summary-actions">
								<!-- IMPORT partials/topic/share-menu.tpl -->
								<!-- IF sourceUrl -->
								<!-- IF showRead -->
								<button id="readability-trigger" 
								   		class="btn--n btn--p read" 
								   		data-url="{sourceUrl}" 
								   		data-title="{title}"><span>Read</span></button>
								<!-- ENDIF showRead -->
								<!-- ENDIF sourceUrl -->
								<button class="btn--n btn--p like disabled">
									<span><i class="icon icon-hand-like-1"></i></span>
								</button>
							</div>
									
							<div class="topic-header-wrapper<!-- IF !image --> no-img<!-- ENDIF !image -->">
								 
								<!-- //TOPIC IMAGE OVERLAY + IMAGE CLIP -->
								<!-- IF image -->
								<div class="topic-cover-image cover position hidden" 
									 style="background-image: url({image});">
									<img class="topic-img hidden" src="{image}" alt="{title}" title="{title}">
								</div>
								<!-- ENDIF image -->
								
								<!-- //TOPIC SUMMARY -->
								<div class="topic-summary">
									<!-- IF snipet -->
									<p class="topic-summary-content">{snipet}</p>
									<!-- ENDIF snipet -->
								
									<div class="topic-summary-op">
										<!-- IF showRead -->
										<!-- BEGIN posts -->
										<!-- IF @first -->
										<div class="topic-summary-op-img cover position" 
											 style="background-image:url({posts.user.picture});"></div>
										<div class="topic-summary-op-main">
											<div class="topic-summary-op-header">
												<a href="{config.relative_path}/user/{posts.user.userslug}" class="topic-summary-op-username">{posts.user.username}</a> 
												<small class="timeago relativeTime" title="{relativeTime}"></small>
											</div>
											<div class="topic-summary-op-about">
												{posts.content}							
											</div>
										</div>
										<!-- ENDIF @first -->
										<!-- END posts -->
										<!-- ELSE -->
										
										<!-- BEGIN posts -->
										<!-- IF @first -->
										<div class="topic-summary-op-img cover position" 
											 style="background-image:url({posts.user.picture});"></div>
										<div class="topic-summary-op-main">
											<div class="topic-summary-op-header">
												<a href="{config.relative_path}/user/{posts.user.userslug}" class="topic-summary-op-username">{posts.user.username}</a> 
												<small class="timeago relativeTime" title="{relativeTime}"></small>
											</div>
											<!-- ENDIF @first -->
											<!-- END posts -->
											<div class="topic-summary-op-about">
												<p>{title}</p>						
											</div>
										</div>
										<!-- ENDIF showRead -->
									</div>
									
									
									<!-- @NOTE:
										 Do not remove this as it will be moved
										 in place for the comments. There was
										 a misunderstnding as to what this would
										 be placed here.
									
									<div class="topic-comment-wrapper __ hide">
										<div class="topic-comment-img"></div>
										<div class="topic-comment">
											<div class="topic-comment-heading">
												<div class="topic-comment-who">{username}</div>
												<div class="topic-comment-when">{time}</div>
											</div>
											<div class="topic-comment-what">{content}</div>
										</div>
									</div>
									-->
									
								</div>
								<!-- //TOPIC ARTICLE IMG -->
								<!-- IF sourceUrl -->
								<div class="topic-article-gutter">
									<a href="{sourceUrl}" target="_blank" class="topic-article-img cover" style="background-image:url({image});"></a>
								</div>
								<!-- ENDIF sourceUrl -->
								<!-- //TOPIC TITLE -->
								<h1 class="topic-title" component="post/header" itemprop="name">
							
									<!-- @NOTE: 
										 The following below has been commented
										 out until further notice. Do not delete
										 this section unless notified.
									<!--i class="fa fa-thumb-tack <!-- IF !pinned -->hidden<!-- ENDIF !pinned -->"></i>
									<i class="fa fa-lock <!-- IF !locked -->hidden<!-- ENDIF !locked -->"></i-->
							
							
									<!-- IF sourceUrl -->
									<a href="{sourceUrl}" target="_blank">
										<span component="topic/title">{title}</span>
									</a>
									<!-- ELSE -->
									<!-- BEGIN posts -->
									<!-- IF @first -->
									<a href="#">
										<span component="topic/title">{posts.content}</span>
									</a>
									<!-- ENDIF @first -->
									<!-- END posts -->
									<!-- ENDIF sourceUrl -->
									
									
									<!-- IMPORT_disabled partials/topic/sort.tpl -->
							
									<div class="__ hide">
									<!--button component="topic/follow" class="btn hidden-xs hidden-sm <!-- IF isFollowing -->hidden<!-- ENDIF isFollowing -->">
										<span>[[topic:watch]]</span> <i class="fa fa-eye"></i>
									</button>
							
									<button component="topic/unfollow" class="btn hidden-xs hidden-sm <!-- IF !isFollowing -->hidden<!-- ENDIF !isFollowing -->">
										<span>[[topic:unwatch]]</span> <i class="fa fa-eye-slash"></i>
									</button>
							
									<span class="browsing-users hidden hidden-xs hidden-sm pull-right">
										<span>[[category:browsing]]</span>
										<div component="topic/browsing/list" class="thread_active_users active-users inline-block"></div>
										<small class="hidden">
											<i class="fa fa-users"></i> <span component="topic/browsing/count" class="user-count"></span>
										</small>
									</span-->
									</div>
									
								</h1>
								
								
							</div>
							
							<!-- //TOPIC REPLY -->
							<div class="topic-reply" id="cmp-uuid-tap-into-plugin-mentions">
								<div class="topic-reply-wrapper">
									<div class="topic-reply-img cover" 
										 style="background-image:url(//storage.googleapis.com/jotter/img/avatar-200x200_v1.png);"></div>
									<div class="topic-reply-title"></div>
									<textarea placeholder="Leave a comment..." class="write"></textarea>
								</div>
								<div class="topic-reply-comment">
									<button class="btn--n topic-reply-btn" data-tid="{tid}">
										<span>Comment</span>
									</button>
								</div>
							</div>
							
							
							<div class="topic-statistics clearfix">
								<div class="topic-statistics-left">
									<!-- IF unreplied -->
									No replies
									<!-- ELSE -->
									<span class="posts">{postcount}</span> replies
									<!-- ENDIF unreplied -->
								</div>
								<div class="topic-statistics-right">
									<span class="views">{viewcount}</span> views
								</div>
							</div>
							
										
							<div component="topic/deleted/message" 
								 class="alert alert-warning<!-- IF !deleted --> hidden<!-- ENDIF !deleted -->">[[topic:deleted_message]]</div>
									
							<ul component="topic" class="posts<!-- IF unreplied --> unreplied<!-- ENDIF unreplied -->" data-tid="{tid}">
								<!-- BEGIN posts -->
									<li component="post" class="clearfix<!-- IF posts.deleted --> deleted<!-- ENDIF posts.deleted -->" <!-- IMPORT partials/data/topic.tpl -->>
										<a component="post/anchor" name="{posts.index}"></a>
						
										<meta itemprop="datePublished" content="{posts.relativeTime}">
										<meta itemprop="dateModified" content="{posts.relativeEditTime}">
						
										<!-- IMPORT partials/topic/post.tpl -->
									</li>
									<!-- IF unreplied -->
									<li class="posts-unreplied">
										<span class="message">No one has replied to this topic yet. <em>Be the first!</em></span>
									</li>
									<!-- ENDIF unreplied -->
								<!-- END posts -->
							</ul>
							

							<div class="post-bar bottom-post-bar <!-- IF unreplied -->hidden<!-- ENDIF unreplied -->">
								<!-- IMPORT_disabled partials/post_bar.tpl -->
							</div>
					
							<!-- IF config.usePagination -->
							<!-- IMPORT partials/paginator.tpl -->
							<!-- ENDIF config.usePagination -->
					
							<!-- IMPORT partials/move_thread_modal.tpl -->
							<!-- IMPORT partials/fork_thread_modal.tpl -->
							<!-- IMPORT partials/move_post_modal.tpl -->
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<!-- @NOTE: 
	 Move this JS code below in the main
	 JS script for the site. This is
	 temporary and should be moved as
	 per; review + refactor if possible.
-->
<script>
	$('.topic-reply-img.cover').css('background-image', 'url(' + app.user.picture + ')');
	$('textarea').focus( function() {
		$('.topic-reply').addClass('is-open');
	});
	$('textarea').blur( function() {
		$('.topic-reply').removeClass('is-open');
	});
	require(['autosize'], function(autosize) {
		autosize($('textarea'));
	});
</script>
	 
<!-- IMPORT partials/noscript/paginator.tpl -->
<!-- IMPORT partials/variables/topic.tpl -->
<!-- IMPORT partials/readability_modal.tpl -->
