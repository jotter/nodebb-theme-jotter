<div class="everything">
	<div class="everything-community">
		<div class="everything-header __ hide">
			<div class="everything-img everything-cover everything-img-withfilter everything-position"
				 style="background-image:url({image});">
				<div class="everything-holder everything-cover everything-position" 
					 style="background-image:url({image});"></div>
				<div class="everything-overlay everything-position"></div>
			</div>
			
			<div class="community-cover container">
				<div class="community-title">
					<h1 class="community-heading">
						<span class="community-subtitle hide">The latest in</span>
						<!-- @todo: functionality
							 From the API, it would be {name},
							 but this does not work from the
							 API for some reason.
						-->
						<span class="community-name">{name}</span>
					</h1>
				</div>
			</div>
		</div>

		<div class="everything-container">
			<div class="container">
				<div class="col-md-10 col-md-offset-1">
					
					<header class="u-offset">
						<div class="content-navigation centered">
							<div class="content-navigation-item url active">{name}</div>
							

							<div class="member-button text-center">
								<span class="btn--n <!-- IF !isMember -->hide<!-- ENDIF !isMember -->" 
											id="leave-btn" data-cid="{cid}">
									<span class="textual">Leave</span>
								</span>
								<span class="btn--n <!-- IF isMember -->hide<!-- ENDIF isMember -->" 
											id="join-btn" data-cid="{cid}" >
									<span class="textual">Join</span>
								</span>

								<!-- IMPORT partials/community/new_topic_button.tpl -->
								
							</div>

						</div>
					</header>
					
					<div class="community _gpu">

							<!-- IMPORT partials/contextual_search.tpl -->
							
							<div class="community-container">
								<div class="community-communities-container clearfix _ hide">
									<!-- <h3 class="content-heading">Communities who share your interest</h3> -->
									<!-- @todo: implementation/functionality
										 We do not have this implemented yet in the API,
										 so for now we will render placeholders.
									-->
									<!-- //IMPORT partials/community/communities/card.tpl -->
								</div>
								
								<div class="community-conversations-container clearfix">
									<!-- <h3 class="content-heading">Conversations related to your interest</h3> -->
									<div id="community-container">
										<!-- BEGIN topics -->
										<div class="community-conversations col-md-4 col-sm-6 col-xs-12">
											<!-- IMPORT partials/community/conversations/card.tpl -->
										</div>	
										<!-- END topics -->
									</div>	
								</div>
								
								<div class="community-people-container clearfix _ hide">
									<h3 class="content-heading">People who share your interest</h3>
									<!-- @todo: implementation/functionality
										 We do not have this implemented yet in the API,
										 so for now we will render placeholders.
									-->
									<!-- //IMPORT partials/community/people/card.tpl -->
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- DO NOT REMOVE BELOW -->

<!-- IMPORT partials/variables/category.tpl -->

<input type="hidden" template-variable="nextStart" value="{nextStart}" />
<input type="hidden" template-variable="communitySlug" value="{slug}" />
<input type="hidden" template-variable="communityId" value="{cid}" />

<script>
	'use strict';
	require(['forum/community','forum/jinfinitescroll'], function(community, scroll) {
    setTimeout(function() {
      $('.contextual--search-input').attr('placeholder', 'Search for conversations in {name}');
      community.init();
      scroll.init('category');
    }, 300);
	});

	//# sourceURL=category.js
</script>
