<!-- IMPORT partials/account_menu.tpl -->
<div class="col-md-10 col-md-offset-1 no-p-lr">
	<div class="account">

		<div class="row">
			<div class="col-md-12 account-block">

				<div id="collapse_user_stats" class="collapse">
					<div class="account-picture-block panel panel-default">
						<div class="panel-body">
								<span class="text-center"><h4>User Stats</h4></span>
								<hr/>
								<div class="text-center account-stats">
									<div class="inline-block text-center">
										<span class="human-readable-number" title="{postcount}">{postcount}</span>
										<span class="account-bio-label">[[global:posts]]</span>
									</div>

									<div class="inline-block text-center">
										<span class="human-readable-number" title="{profileviews}">{profileviews}</span>
										<span class="account-bio-label">[[user:profile_views]]</span>
									</div>
								</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-body text-center">

							<span class="account-bio-label">[[user:followers]]</span>
							<span class="human-readable-number account-bio-value" title="{followerCount}">{followerCount}</span>

							<span class="account-bio-label">[[user:following]]</span>
							<span class="human-readable-number account-bio-value"  title="{followingCount}">{followingCount}</span>

							<span class="account-bio-label">[[user:joined]]</span>
							<span class="timeago account-bio-value" title="{joindate}"></span>

							<span class="account-bio-label">[[user:lastonline]]</span>
							<span class="timeago account-bio-value" title="{lastonline}"></span>
						</div>
					</div>
				</div>
					
			</div>

			<div class="col-md-12">
			<!-- IF !posts.length -->
			<span>[[user:has_no_posts]]</span>
			<!-- ENDIF !posts.length -->
			<!-- IMPORT partials/posts_list.tpl -->
			</div>

		</div>

		<br/>
		<div id="user-action-alert" class="alert alert-success hide"></div>

	</div>
</div>
<!-- IMPORT partials/variables/account.tpl -->
<!-- IMPORT partials/variables/account/profile.tpl -->