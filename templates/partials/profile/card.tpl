<div class="community-card-cover">
	<div class="community-card-container clearfix"
    <!-- IF !posts.topic.image -->style="height:100%"<!-- ENDIF !posts.topic.image -->>

		<!-- //COMMUNITY CARD COVER + TITLE WITH/PROFILE IMG -->
		<div class="community-card-img with-img"
      <!-- IF !posts.topic.image -->style="height:100%"<!-- ENDIF !posts.topic.image -->>
		
			<!-- @todo: functionality 
				 1. timestamp is not working properly,
				 perhaps it needs {relativeTime} via API
			-->
			<div class="community-card-timeago timeago" title="{timestamp}"></div>
			<div class="community-card-actions hide">
				<!-- //COMMUNITY CARD ACTIONS 
					 @todo: functionality
					 [x] 1. use ".community-card-*" * for JS events
					 [x] 2. not working
					 [ ] 3. create new partial /card-actions template
					 [/] 4. add number of ? in .numerical
				
				@REMOVE:
				
				<a class="community-card-btn community-card-jot content-card-jot" href="#" data-title="{postData.title}" 
						data-toggle="modal" data-target="#jot-widget" data-pid="{postData.pid}">
			    <span class="textual jot"><i class="icon jotter-add"></i> Jot </span>
			    <span class="numerical">0</span>
				</a>
				
	

				<a class="community-card-btn content-card-save" href="#" component="post/favourite" data-pid="{postData.pid}" data-title="{postData.title}" data-favourited="">
				    <span class="favourite-action"> Save </span>
				    <span class="unfavourite-action"> Saved </span>
				</a>
				
				-->
				
				<!-- @todo: regression, functionality
					
				@DEFER/REMOVE: 
				
				<div class="community-dropdown dropdown" component="post/tools">
					<a class="community-card-btn" href="#" data-toggle="dropdown" aria-expanded="false">
				    	<span><i class="icon jotter-send"></i> Share</span>
					</a>
					<div class="dropdown-menu text-center" role="menu">
					<div class="dropdown-arrow"></div>
						<a role="menuitem" class="instant-facebook-share" tabindex="-1" href="#">
							<span>Share via Facebook</span>
						</a>
						<a role="menuitem" class="instant-twitter-share" tabindex="-1" href="#">
							<span>Share via Twitter</span>
						</a>
						<a role="menuitem" class="instant-email-share" tabindex="-1" href="#">
							<span>Share via Email</span>
						</a>
					</div>
				</div>
				
				-->
			</div>
					
			<!-- //COMMUNITY CARD TITLE 
				
				 @todo: implement
				 1. replace href url with API data
				 2. replace content with API data
				 3. replace op href url with API data (profile)
				 4. replace timeago href url with API data (topic)
			-->

			<div class="community-card-inner" data-href="{config.relative_path}/topic/{posts.topic.slug}">
				<div class="community-card-title">
					<a class="community-card-author" href="#">
						<img src="{posts.user.picture}" 
						     onerror="this.src ='//storage.googleapis.com/jotter/img/avatar-40x40_v1.png'">
					</a>
					<div class="community-card-said">
						<h3>
              {posts.user.username}
              <span class="community-card-time timeago relativeTime" title="{posts.relativeTime}">
                {posts.relativeTime}
              </span>
            </h3>
					</div>
          <span class="community-card-article-title">{posts.title}</span>

          <!-- //COMMUNITY CARD CONTENT -->
          <div class="community-card-content content">
            {posts.content}
          </div>
				</div>

        <div class="community-card-comments">{posts.topic.postcount}</div>
        <span class="community-card-views">
          <i class="icon icon-preview-1-1"></i> {posts.topic.viewcount} views
        </span>

			</div>
		</div>
	
		<!-- //COMMUNITY CARD FOOTER -->
    <!-- IF posts.topic.image -->
		<div class="community-card-footer">
			<!-- @todo: implementation
				 1. replace href url with API data (topic)
			-->

			<a class="community-card-relation" href="{posts.topic.sourceUrl}" target="_blank" style="background-image:url({posts.topic.image})"></a>
			<a class="source-attribution-link" href="{posts.topic.sourceUrl}" target="_blank"></a>
    </div>
    <!-- ENDIF posts.topic.image -->
	</div>
	<!-- //COMMUNITY CARD META -->
</div>
<!-- //COMMUNITY CARD -->
