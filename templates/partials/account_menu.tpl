<!-- //PROFILE HEADER -->
<div class="everything-header" data-userslug="{userslug}" data-uid="{uid}">
	<div class="everything-account account">

		<!-- //PROFILE AVATAR -->
		<div class="profile--img">
			<div class="profile--img-holder cover" style="background-image:url({picture})"></div>
		</div>
		
			
		<!-- //PROFILE USERNAME -->
		<a href="{config.relative_path}/user/{userslug}">
			<!-- IF fullname -->
			<span class="profile--username profile--fullname">{fullname}</span>
			<!-- ELSE -->
			<span class="profile--username account-username">{username}</span>
			<!-- ENDIF fullname -->
		</a>
		
		<!-- //PROFILE DESCRIPTION -->
		<div class="profile--description __ hide">{signature}</div>
		
		<!-- //PROFILE ACTIONS -->
		<div class="profile--actions">
		<!-- IF isSelf -->
		<a class="btn btn-rounded btn-white m-b" href="#" onclick="app.logout()">
			<i class="fa fa-fw fa-sign-out"></i><span> [[global:logout]]</span>
		</a>
		<!-- ELSE -->

		<a href="#" data-toggle="collapse" data-target="#collapse_user_stats"> Stats &nbsp;&nbsp;&nbsp;&nbsp;</a>
		<!-- IF !config.disableChat -->
		<!-- <a id="chat-btn" href="#" class="btn btn-rounded btn-white re m-r-xs">[[user:chat]]</a> -->
		<!-- ENDIF !config.disableChat -->
		<a id="follow-btn" href="#" class="btn btn-rounded btn-primary re not-following<!-- IF isFollowing -->hide<!-- ENDIF isFollowing -->">[[user:follow]]</a>
		<a id="unfollow-btn" href="#" class="btn btn-rounded btn-primary re not-following <!-- IF !isFollowing -->hide<!-- ENDIF !isFollowing -->">[[user:unfollow]]</a>
		<!-- ENDIF isSelf -->
		</div>
			
		
		<!-- IF banned -->
		<div class="__">
			<span class="label label-danger">[[user:banned]]</span>
		</div>
		<!-- ENDIF banned -->
		
		<!-- //PROFILE NAVIGATION -->
	<!-- 	<div class="profile--nav clearfix">
			<a href="{config.relative_path}/user/{userslug}/following" 
			   class="profile--stats following col-lg-3 col-md-3 col-sm-3 col-xs-3 human-readable-number"
			   title="{followingCount}">
			{followingCount}
			</a>
			<a href="{config.relative_path}/user/{userslug}/followers" 
			   class="profile--stats followers col-lg-3 col-md-3 col-sm-3 col-xs-3 human-readable-number">
			{followerCount}
			</a>
			<a href="{config.relative_path}/user/{userslug}/saved"
			   class="profile--stats saved col-lg-3 col-md-3 col-sm-3 col-xs-3 human-readable-number">
			{favcount}
			</a>
			<a href="{config.relative_path}/user/{userslug}/jots"
			   class="profile--stats jots col-lg-3 col-md-3 col-sm-3 col-xs-3 human-readable-number">
			{jotscount}
			</a>
		</div> -->
					
		<div class="clearfix"></div>
	</div>
</div>