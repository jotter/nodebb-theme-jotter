<!-- //POST AVATAR -->
<div class="post-avatar" style="background-image:url({posts.user.picture});">
	<a class="post-avatar-link" href="<!-- IF posts.user.userslug -->{config.relative_path}/user/{posts.user.userslug}<!-- ELSE -->#<!-- ENDIF posts.user.userslug -->"></a>
	<div class="post-avatar-border"></div>
		<img src="{posts.user.picture}" itemprop="image" />
		<!-- IF posts.user.banned -->
		<span class="label label-danger">[[user:banned]]</span>
		<!-- ENDIF posts.user.banned -->
</div>
<!-- //POST WRAPPER -->
<div class="post-wrapper">
	<!-- //POST INFORMATION -->
	<div class="post-information">
		<!-- @todo: reimplement
			 This needs to be moved and attached
			 to the avatar. It has been hidden 
			 for now and is on the todo list.
			 
		<i component="user/status" class="fa fa-circle status {posts.user.status}" title="[[global:{posts.user.status}]]"></i>
		-->
		
		<!-- //POST NAME/USERNAME -->
		<div class="post-name">
			<a class="post-username" href="<!-- IF posts.user.userslug -->{config.relative_path}/user/{posts.user.userslug}<!-- ELSE -->#<!-- ENDIF posts.user.userslug -->" itemprop="author" data-username="{posts.user.username}" data-uid="{posts.user.uid}">{posts.user.username}</a>
		</div>
		
		<!-- //POST MISC -->
		<div component="post/editor" class="post-edited <!-- IF !posts.editor.username -->hidden<!-- ENDIF !posts.editor.username -->">[[global:last_edited_by_ago, <strong><a href="{config.relative_path}/user/{posts.editor.userslug}">{posts.editor.username}</a></strong>, <span class="timeago" title="{posts.relativeEditTime}"></span>]]</div>

		<!-- IMPORT partials/topic/badge.tpl -->

		<div class="post-time visible-xs-block visible-sm-inline-block visible-md-inline-block visible-lg-inline-block">
			replied <a class="permalink" href="{config.relative_path}/topic/{slug}/{function.getBookmarkFromIndex}"><span class="timeago" title="{posts.relativeTime}"></span></a>

			<!-- @TODO:
				 We should disable this for now
				 and reimplement these buttons
				 when ready.
			-->
			<!--span class="post-tools">
				<a component="post/reply" class="no-select <!-- IF !privileges.topics:reply -->hidden<!--ENDIF !privileges.topics:reply -->">[[topic:reply]]</a>
				<a component="post/quote" class="no-select <!-- IF !privileges.topics:reply -->hidden<!--ENDIF !privileges.topics:reply -->">[[topic:quote]]</a>
			</span-->
		</div>

		<div class="votes">
			<!-- IF !reputation:disabled -->
			<a component="post/upvote" href="#" class="<!-- IF posts.upvoted -->upvoted<!-- ENDIF posts.upvoted -->">
				<i class="fa fa-chevron-up"></i>
			</a>
			<span component="post/vote-count" data-votes="{posts.votes}">{posts.votes}</span>
			<!-- IF !downvote:disabled -->
			<a component="post/downvote" href="#" class="<!-- IF posts.downvoted -->downvoted<!-- ENDIF posts.downvoted -->">
				<i class="fa fa-chevron-down"></i>
			</a>
			<!-- ENDIF !downvote:disabled -->
			<!-- ENDIF !reputation:disabled -->
		</div>

		<!-- IMPORT_disabled partials/topic/post-menu.tpl -->
	</div>
	
	<!-- //POST CONTENT -->
	<div class="post-content content" component="post/content" itemprop="text">
		{posts.content}
	</div>
	
	<!-- //POST ACTIONS -->
	<div class="post-expander"></div>
	<div class="post-actions">
		<button class="btn--n btn--sm reply-button"><span>Reply</span></button>
	</div>
</div>
<!-- //END -->