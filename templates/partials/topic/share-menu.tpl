<div class="share dropdown">
	<button class="btn--n" data-toggle="dropdown"><span>Share <i class="icon icon-angle-down"></i></span></button>
	<ul class="dropdown-menu" role="menu">
		<li role="presentation" class="dropdown-heading">[[topic:share_this_post]]</li>
		<li class="dropdown-arrow"></li>
		<a role="menuitem" class="facebook-share" tabindex="-1" href="#">
			<span>Share via Facebook</span>
		</a>
		<a role="menuitem" class="twitter-share" tabindex="-1" href="#">
			<span>Share via Twitter</span>
		</a>
		<a role="menuitem" class="google-share" tabindex="-1" href="#">
			<span>Share via Google+</span>
		</a>
	</ul>
</div>
