<!-- BEGIN following -->  
<input type="hidden" template-variable="theirid" value="{following.uid}" />
<input type="hidden" template-type="boolean" template-variable="isFollowing" value="true" />

<div class="user-item col-lg-3 col-md-3 col-sm-4 col-xs-12" data-uid="{following.uid}">
	<div class="user-item-wrapper">
	
			<!-- IF following.unread -->
			<div class="unread-bg">
	    	<span class="unread">unread</span>
	    </div>
	    <!-- ENDIF following.unread -->
	    
	    <div class="user-avatar-cover">
			<a class="user-avatar-wrapper" style="background:transparent;" href="{config.relative_path}/user/{following.userslug}">				
				<div class="user-avatar-img cover position _ hide" 
					 style="/*background-image:url({following.picture});*/">
				<div class="user-avatar-filter cover position _ hide" 
					 style="/*background-image:url({following.picture});*/"></div>
				</div>
				<!-- @todo: use background avatar
					 @readd: {following.picture/gravatarpicture/image} via API 
					 @readd: //storage.googleapis.com/jotter/img/avatar-200x200_v1.png
				-->
		    </a>
		    <a class="user-avatar-picture cover" 
			   href="{config.relative_path}/user/{following.userslug}"
			   style="background-image:url({following.picture});"></a>
	    </div>
	    
		<div class="user-information">
			<h3 class="user-name ">
				<a class="user-fullname url" href="{config.relative_path}/user/{following.userslug}">
					<span class="account-username">{following.username}</span>
				</a>
			</h3>
			
			<div class="user-collection clearfix __">
				<div class="user-collection-faux main"></div>
				<div class="user-collection-faux"></div>
				<div class="user-collection-faux"></div>
				<div class="user-collection-faux"></div>
				<div class="user-collection-faux"></div>
			</div>
	      
			<div class="user-stats clearfix">
				<div class="user-stats-item posts">
					<!-- IF following.postcount -->{following.postcount}<!-- ELSE -->0<!-- ENDIF following.postcount -->
				</div>
				<div class="user-stats-item followers">
					<!-- IF following.followerCount -->{following.followerCount}<!-- ELSE -->0<!-- ENDIF following.followerCount -->
				</div>
			</div>

			<div class="user-footer">
				<a href="#" class="user-follow-btn follow-btn btn--n hide" data-uid="{following.uid}" >
					<span> <strong class="textual">Follow</strong> </span>
				</a>
				<a href="#" class="user-follow-btn unfollow-btn btn--n" data-uid="{following.uid}">
					<span> <strong class="textual">Following</strong> </span>
				</a>
			</div>

			<div class="user-gutter __ hide">
				<strong class="human-readable-number">{following.profileviews}</strong> views
			</div>
		</div>
	</div>
</div>
<!-- END following -->


