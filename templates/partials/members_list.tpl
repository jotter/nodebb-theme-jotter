<!-- IF !members.length -->  
	No members
<!-- ENDIF !members.length -->

<!-- BEGIN members -->  
<input type="hidden" template-variable="theirid" value="{members.uid}" />
<input type="hidden" template-type="boolean" template-variable="isFollowing" value="false" />

<div class="user-item col-lg-3 col-md-3 col-sm-4 col-xs-12" data-uid="{members.uid}">
	<div class="user-item-wrapper">
	    
	    <div class="user-avatar-cover">
			<a class="user-avatar-wrapper" style="background:transparent;" href="{config.relative_path}/user/{members.userslug}">				
				<div class="user-avatar-img cover position _ hide" 
					 style="/*background-image:url({members.picture});*/">
				<div class="user-avatar-filter cover position _ hide" 
					 style="/*background-image:url({members.picture});*/"></div>
				</div>
				<!-- @todo: use background avatar
					 @readd: {members.picture/gravatarpicture/image} via API 
					 @readd: //storage.googleapis.com/jotter/img/avatar-200x200_v1.png
				-->
		    </a>
		    <a class="user-avatar-picture cover" 
			   href="{config.relative_path}/user/{members.userslug}"
			   style="background-image:url({members.picture});"></a>
	    </div>
	    
		<div class="user-information">
			<h3 class="user-name">
				<a class="user-fullname url" href="{config.relative_path}/user/{members.userslug}">
					<span class="account-username">{members.username}</span>
					<span class="user-username">@{members.userslug}</span>
				</a>
			</h3>
			
			<div class="user-collection clearfix __">
				<div class="user-collection-faux main"></div>
				<div class="user-collection-faux"></div>
				<div class="user-collection-faux"></div>
				<div class="user-collection-faux"></div>
				<div class="user-collection-faux"></div>
			</div>
	      
			<div class="user-stats clearfix">
				<div class="user-stats-item posts">
					<!-- IF postcount -->{members.postcount}<!-- ELSE -->0<!-- ENDIF postcount -->
				</div>
				<div class="user-stats-item followers">
					<!-- IF followingCount -->{members.followingCount}<!-- ELSE -->0<!-- ENDIF followingCount -->
				</div>
			</div>

			<div class="user-footer <!-- IF members.isSelf -->invisible <!-- ENDIF members.isSelf -->">
				<a  href="#" class="user-follow-btn follow-btn btn--n <!-- IF members.following --> hide <!-- ENDIF members.following -->" 
						data-uid="{members.uid}" >
					<span> <strong class="textual">Follow</strong> </span>
				</a>
				<a 	href="#" class="user-follow-btn unfollow-btn btn--n <!-- IF !members.following --> hide <!-- ENDIF !members.following -->" 
						data-uid="{members.uid}">
					<span> <strong class="textual">Following</strong> </span>
				</a>
			</div>
	

			<div class="user-gutter __ hide">
				<strong class="human-readable-number">{members.profileviews}</strong> views
			</div>
		</div>
	</div>
</div>
<!-- END members -->