<!-- BEGIN users -->  
<input type="hidden" template-variable="theirid" value="{users.uid}" />
<input type="hidden" template-type="boolean" template-variable="isFollowing" value="false" />

<div class="user-item col-lg-3 col-md-3 col-sm-4 col-xs-12" data-uid="{users.uid}">
	<div class="user-item-wrapper">
	    
	    <div class="user-avatar-cover">
			<a class="user-avatar-wrapper" style="background:transparent;" href="{config.relative_path}/user/{users.userslug}">				
				<div class="user-avatar-img cover position _ hide" 
					 style="/*background-image:url({users.picture});*/">
				<div class="user-avatar-filter cover position _ hide" 
					 style="/*background-image:url({users.picture});*/"></div>
				</div>
				<!-- @todo: use background avatar
					 @readd: {users.picture/gravatarpicture/image} via API 
					 @readd: //storage.googleapis.com/jotter/img/avatar-200x200_v1.png
				-->
		    </a>
		    <a class="user-avatar-picture cover" 
			   href="{config.relative_path}/user/{users.userslug}"
			   style="background-image:url({users.picture});"></a>
	    </div>
	    
		<div class="user-information">
			<h3 class="user-name">
				<a class="user-fullname url" href="{config.relative_path}/user/{users.userslug}">
					<span class="account-username">{users.username}</span>
				</a>
			</h3>
			
			<div class="user-collection clearfix __">
				<div class="user-collection-faux main"></div>
				<div class="user-collection-faux"></div>
				<div class="user-collection-faux"></div>
				<div class="user-collection-faux"></div>
				<div class="user-collection-faux"></div>
			</div>
	      
			<div class="user-stats clearfix">
				<div class="user-stats-item posts">
					<!-- IF users.postcount -->{users.postcount}<!-- ELSE -->0<!-- ENDIF users.postcount -->
				</div>
				<div class="user-stats-item followers">
					<!-- IF users.followerCount -->{users.followerCount}<!-- ELSE -->0<!-- ENDIF users.followerCount -->
				</div>
			</div>

			<div class="user-footer">
				<a href="#" class="user-follow-btn follow-btn btn--n" data-uid="{users.uid}" >
					<span> <strong class="textual">Follow</strong> </span>
				</a>
				<a href="#" class="user-follow-btn unfollow-btn btn--n hide" data-uid="{users.uid}">
					<span> <strong class="textual">Following</strong> </span>
				</a>
			</div>
	

			<div class="user-gutter __ hide">
				<strong class="human-readable-number">{users.profileviews}</strong> views
			</div>
		</div>
	</div>
</div>
<!-- END users -->