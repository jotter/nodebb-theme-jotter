<!-- IF privileges.topics:create -->
	
	<!-- *** Fixed New Topic Bottom  ***--> 
	<div class="taskbar navbar-fixed-bottom show hidden-xs hidden-sm">
		<div class="navbar-inner">
			<ul class="nav navbar-nav pull-right">
				<li class="taskbar-composer active">
					<a href="#" id="new_topic"><i class="fa fa-plus"></i> <span>New Topic</span></a>
				</li>
			</ul>
		</div>
	</div>
	
	<!-- *** Default New Topic Bottom (for mobile) ***--> 
	<span class="btn--n visible-xs-inline-block visible-sm-inline-block" id="new_topic">
		<span class="textual">[[category:new_topic_button]]</span>
	</span>
	
<!-- ELSE -->
	<!-- IF !loggedIn -->
	<a href="/login?next=category/{slug}" class="btn btn-primary">[[category:guest-login-post]]</a>
	<!-- ENDIF !loggedIn -->
<!-- ENDIF privileges.topics:create -->