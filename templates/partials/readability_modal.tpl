<div id="readability-modal" class="modal fade readability-modal" tabindex="-1" role="dialog"
  aria-labeledby="Read" aria-hidden="true" style="display:none">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <iframe id="readability-iframe"></iframe>
      </div>
    </div>
  </div>
</div>
