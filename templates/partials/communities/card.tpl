<!-- BEGIN categories -->
	<div class="communities-item col-md-4 col-sm-6 col-xs-12">
		<div class="communities-item-wrapper stacked">
		<a class="communities-item-url position" href="{config.relative_path}/community/{categories.slug}"></a>
			<div class="communities-item-cover clearfix">
				
				<div class="communities-item-img cover" 
					 style="background-image:url({categories.image});"></div>
					 
				<div class="communities-item-container">

					<div class="communities-item-information">
						<!-- @todo: implement, functionality
							 When clicking a link from the title, or 
							 a specific click area, it should open 
							 sidebar with streamlined discussion.
							 
							 We are not going to use a modal for this,
							 instead the sidebar will be centered so
							 technically it won't be a sidebar.
						-->
						<a class="communities-item-title" 
						   href="#">
							<span class="communities-item-type hide"></span>
							<h3>{categories.mostRecentTopicTitle}</h3>
						</a>	
					</div>
					
				</div>
				
				<div class="communities-item-footer">
					<!-- @todo: implement
						 Upon click, this should take user to the 
						 individual community page where a user
						 will be able to see all of the latest 
						 in that particular community.
					-->
					<a class="communities-item-name url" href="{config.relative_path}/community/{categories.slug}">{categories.name}</a>
					
					<!-- @todo: implement
						 Using the API endpoints, plugin 
						 the statistics for the the following:
						 1.) conversations
						 2.) members 
					-->
					<div class="communities-item-analytics">
						<div class="communities-item-stats conversations">
							{categories.topic_count}
						</div>
						
						<!-- @todo: endpoint required -->
						<div class="communities-item-stats members">
							<!-- IF categories.membersCount -->{categories.membersCount}
							<!-- ELSE --> 0
							<!-- ENDIF categories.membersCount -->
							<a href="{config.relative_path}/community/{categories.slug}/members">
								Members
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END categories -->