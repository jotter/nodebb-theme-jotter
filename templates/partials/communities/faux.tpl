<style>@import url("/fonts/_b/_br/wf/f.css");</style>
<!-- //FAUX -->
<div class="communities-item communities-faux _faux col-md-4 col-sm-6 col-xs-12">
	<div class="communities-item-wrapper">
		<div class="communities-item-cover clearfix">
			<div class="communities-item-img cover" 
				 style="background-image:url(//storage.googleapis.com/jotter/jotter-icon-w.png);"></div>
			<div class="communities-item-container">
				<div class="communities-item-information">
					<a class="communities-item-title" 
					   href="#">
						<span class="communities-item-type hide"></span>
						<h3>- --- - ---</h3>
					</a>	
				</div>
			</div>
			<div class="communities-item-footer">
				<a class="communities-item-name url" href="#">-</a>
				<div class="communities-item-analytics">
					<div class="communities-item-stats conversations">
						-
					</div>
					<div class="communities-item-stats members disabled">
						-
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="communities-item communities-faux _faux col-md-4 col-sm-6 col-xs-12 hidden-xs">
	<div class="communities-item-wrapper">
		<div class="communities-item-cover clearfix">
			<div class="communities-item-img cover" 
				 style="background-image:url(//storage.googleapis.com/jotter/jotter-icon-w.png);"></div>
			<div class="communities-item-container">
				<div class="communities-item-information">
					<a class="communities-item-title" 
					   href="#">
						<span class="communities-item-type hide"></span>
						<h3>--- -- ----- ---</h3>
					</a>	
				</div>
			</div>
			<div class="communities-item-footer">
				<a class="communities-item-name url" href="#">--</a>
				<div class="communities-item-analytics">
					<div class="communities-item-stats conversations">
						-
					</div>
					<div class="communities-item-stats members disabled">
						-
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="communities-item communities-faux _faux col-md-4 col-sm-6 col-xs-12 hidden-sm hidden-xs">
	<div class="communities-item-wrapper">
		<div class="communities-item-cover clearfix">
			<div class="communities-item-img cover" 
				 style="background-image:url(//storage.googleapis.com/jotter/jotter-icon-w.png);"></div>
			<div class="communities-item-container">
				<div class="communities-item-information">
					<a class="communities-item-title" 
					   href="#">
						<span class="communities-item-type hide"></span>
						<h3>- ---- -- ----</h3>
					</a>	
				</div>
			</div>
			<div class="communities-item-footer">
				<a class="communities-item-name url" href="#">--</a>
				<div class="communities-item-analytics">
					<div class="communities-item-stats conversations">
						-
					</div>
					<div class="communities-item-stats members disabled">
						-
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- //FAUX -->
<div class="communities-item communities-faux _faux col-md-4 col-sm-6 col-xs-12 hidden-sm hidden-xs">
	<div class="communities-item-wrapper">
		<div class="communities-item-cover clearfix">
			<div class="communities-item-img cover" 
				 style="background-image:url(//storage.googleapis.com/jotter/jotter-icon-w.png);"></div>
			<div class="communities-item-container">
				<div class="communities-item-information">
					<a class="communities-item-title" 
					   href="#">
						<span class="communities-item-type hide"></span>
						<h3>- --- - ---</h3>
					</a>	
				</div>
			</div>
			<div class="communities-item-footer">
				<a class="communities-item-name url" href="#">-</a>
				<div class="communities-item-analytics">
					<div class="communities-item-stats conversations">
						-
					</div>
					<div class="communities-item-stats members disabled">
						-
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="communities-item communities-faux _faux col-md-4 col-sm-6 col-xs-12 hidden-sm hidden-xs">
	<div class="communities-item-wrapper">
		<div class="communities-item-cover clearfix">
			<div class="communities-item-img cover" 
				 style="background-image:url(//storage.googleapis.com/jotter/jotter-icon-w.png);"></div>
			<div class="communities-item-container">
				<div class="communities-item-information">
					<a class="communities-item-title" 
					   href="#">
						<span class="communities-item-type hide"></span>
						<h3>--- -- ----- ---</h3>
					</a>	
				</div>
			</div>
			<div class="communities-item-footer">
				<a class="communities-item-name url" href="#">--</a>
				<div class="communities-item-analytics">
					<div class="communities-item-stats conversations">
						-
					</div>
					<div class="communities-item-stats members disabled">
						-
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="communities-item communities-faux _faux col-md-4 col-sm-6 col-xs-12 hidden-sm hidden-xs">
	<div class="communities-item-wrapper">
		<div class="communities-item-cover clearfix">
			<div class="communities-item-img cover" 
				 style="background-image:url(//storage.googleapis.com/jotter/jotter-icon-w.png);"></div>
			<div class="communities-item-container">
				<div class="communities-item-information">
					<a class="communities-item-title" 
					   href="#">
						<span class="communities-item-type hide"></span>
						<h3>- ---- -- ----</h3>
					</a>	
				</div>
			</div>
			<div class="communities-item-footer">
				<a class="communities-item-name url" href="#">--</a>
				<div class="communities-item-analytics">
					<div class="communities-item-stats conversations">
						-
					</div>
					<div class="communities-item-stats members disabled">
						-
					</div>
				</div>
			</div>
		</div>
	</div>
</div>