// Global Jotter stuff
$(window).on('action:ajaxify.contentLoaded', function() {
  'use strict';


  //// "Constants"

  var DEFAULT_ALERT_TIMEOUT = 3000;
  var CUTOFF_HEIGHT = 60;


 /// Workaround for Composer
  $(window).on('action:composer.topics.post', function(ev, data) {
    ajaxify.go('community/' + data.data.category.slug);
    history.back();
  });
  $(window).on('action:ajaxify.end', function(ev, data) {
    $('.composer-discard:eq(0):not([data-discard-clicked])')
      .attr('data-discard-clicked', 1)
      .click();
  });


// FIX! Facebook's OAuth 2.0 implementation has a [bug][1] in which the fragment `#_=_` is appended to the callback URL.
 if (window.location.hash == '#_=_'){
    history.replaceState
        ? history.replaceState(null, null, window.location.href.split('#')[0])
        : window.location.hash = '';
  }



  //// Workaround for Bootstrap modal
  function preventDefault(e) {
    e.preventDefault();
  }
  function disableScrolling() {
    $('body').on('scroll touchmove mousewheel', preventDefault);
  }
  function enableScrolling() {
    $('body').off('scroll touchmove mousewheel', preventDefault);
  }
  $(document)
    .off('show.bs.modal', '*', disableScrolling)
    .on( 'show.bs.modal', '*', disableScrolling)
    .off('hide.bs.modal', '*', enableScrolling)
    .on( 'hide.bs.modal', '*', enableScrolling);



  //// Readability
  $(document)
    .off('click', '#readability-trigger')
    .on('click', '#readability-trigger', function() {
      var $t = $(this);

      // Empty iframe
      $('#readability-iframe')
        .replaceWith(
          $('iframe')
            .attr('id', 'readability-iframe')
            .attr('src', 'data:text/html;charset=utf-8,')
        );

      // Show modal
      $('#readability-modal').modal('show');

      // Get readable content
      $.get('/read/' + encodeURIComponent($t.attr('data-url')), function(data) {
        var body = document.createElement('body');
        body.innerHTML = data;
        var toRemove = []
          .concat([].slice.call(body.querySelectorAll('meta')))
          .concat([].slice.call(body.querySelectorAll('script')))
          .concat([].slice.call(body.querySelectorAll('footer')))
          //.concat([].slice.call(body.querySelectorAll('link')))
          .concat([
            body.querySelector('header'),
            body.querySelector('article header small')
          ]);

        toRemove.forEach(function(item) {
          if (item && item.parentNode) {
            item.parentNode.removeChild(item);
          }
        });

        // Display processed content
        // Replace iframe element, so no history state is pushed
        // (like when modifying src)
        $('#readability-iframe')
          .replaceWith(
            $('iframe')
              .attr('id', 'readability-iframe')
              .attr('src', 'data:text/html;charset=utf-8,' + body.innerHTML)
          );

      });

    });



    //// Mentions

    $(window).on('action:topic.loaded', function() {
      $(window).trigger('action:composer.loaded', { post_uuid: 'tap-into-plugin-mentions' });
    });


    //// Topic reply

    $(document)
      .off('click', '.topic-reply button')
      .on('click', '.topic-reply button', function() {
        var $t = $(this);
        socket.emit('posts.reply', {
          tid: parseInt($t.attr('data-tid'), 10),
          content: $t.parents('.topic-reply').find('textarea').val(),
          lock: false
        }, function(err, data) {
          if (err) {
             app.alert({
               message: err.message,
               type: 'danger',
               timeout: DEFAULT_ALERT_TIMEOUT
             });
          }
          else {
            $t.parents('.topic-reply').find('textarea').val('');
            $('.posts.unreplied li.posts-unreplied').remove();
            $('.posts').removeClass('unreplied');
          }
        });
      });

    //// Reply to person
    $(document)
      .off('click', '[component="post"] .reply-button')
      .on('click', '[component="post"] .reply-button', function() {
        $('.topic-reply textarea')
          .val('@' + $(this).parents('li').attr('data-userslug') + ' ')
          .focus();
        $('html, body').scrollTop($('.topic-reply textarea').offset().top - 75);
      });




    //// "read more" / "read less" links
    $('.posts .post-content').each(function() {
      var $t = $(this);
      if ($t.height() > CUTOFF_HEIGHT) {
        $t.css('max-height', CUTOFF_HEIGHT);
        $t.next().prepend(
          $('<a>')
            .attr('href', '#')
            .attr('data-expanded', 1)
            .addClass('read-more clearfix')
            .on('click', function() {
              if ($(this).attr('data-expanded')) {
                $(this).text('read more...').attr('data-expanded', '');
                $t
                  .css('max-height', CUTOFF_HEIGHT)
                  .removeClass('is-expanded');
              }
              else {
                $(this).text('...read less').attr('data-expanded', 1);
                $t
                  .css('max-height', 'none')
                  .addClass('is-expanded');
              }
            })
            .trigger('click')
        );
      }
    });



  //// via "example.com" links

  function onSourceAttributionLink(e) {
    if (e.target.href) {
      e.target.innerHTML = e.target.hostname;
    }
  }
  document.removeSelectorListener('.source-attribution-link', onSourceAttributionLink);
  document.addSelectorListener('.source-attribution-link', onSourceAttributionLink);




  //// handle .community-card-inner click
  // (covering the area with a link hijack hover event)

  $(document)
    .off('click', '.community-card-inner')
    .on('click', '.community-card-inner', function() {
      location = $(this).attr('data-href');
    });


});









//// https://github.com/csuwildcat/SelectorListener/blob/master/selector-listeners.js

(function(){

	var events = {},
		selectors = {},
		styles = document.createElement('style'),
		keyframes = document.createElement('style'),
		head = document.getElementsByTagName('head')[0],
		startNames = ['animationstart', 'oAnimationStart', 'MSAnimationStart', 'webkitAnimationStart'],
		startEvent = function(event){
			event.selector = (events[event.animationName] || {}).selector;
			((this.selectorListeners || {})[event.animationName] || []).forEach(function(fn){
				fn.call(this, event);
			}, this);
		},
		prefix = (function() {
			var duration = 'animation-duration: 0.001s;',
				name = 'animation-name: SelectorListener !important;',
				computed = window.getComputedStyle(document.documentElement, ''),
				pre = (Array.prototype.slice.call(computed).join('').match(/moz|webkit|ms/)||(computed.OLink===''&&['o']))[0];
			return {
				css: '-' + pre + '-',
				properties: '{' + duration + name + '-' + pre + '-' + duration + '-' + pre + '-' + name + '}',
				keyframes: !!(window.CSSKeyframesRule || window[('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1] + 'CSSKeyframesRule'])
			};
		})();

	styles.type = keyframes.type = "text/css";
	head.appendChild(styles);
	head.appendChild(keyframes);

	HTMLDocument.prototype.addSelectorListener = HTMLElement.prototype.addSelectorListener = function(selector, fn){
		var key = selectors[selector],
			listeners = this.selectorListeners = this.selectorListeners || {};

		if (key) events[key].count++;
		else {
			key = selectors[selector] = 'SelectorListener-' + new Date().getTime();
			var node = document.createTextNode('@' + (prefix.keyframes ? prefix.css : '') + 'keyframes ' + key + ' {'
				+'from { outline-color: #fff; } to { outline-color: #000; }'
			+ '}');
			keyframes.appendChild(node);
			styles.sheet.insertRule(selector + prefix.properties.replace(/SelectorListener/g, key), 0);
			events[key] = { count: 1, selector: selector, keyframe: node, rule: styles.sheet.cssRules[0] };
		}

		if (listeners.count) listeners.count++;
		else {
			listeners.count = 1;
			startNames.forEach(function(name){
				this.addEventListener(name, startEvent, false);
			}, this);
		}

		(listeners[key] = listeners[key] || []).push(fn);
	};

	HTMLDocument.prototype.removeSelectorListener = HTMLElement.prototype.removeSelectorListener = function(selector, fn){
		var listeners = this.selectorListeners || {},
			key = selectors[selector],
			listener = listeners[key] || [],
			index = listener.indexOf(fn);

		if (index > -1){
			var event = events[selectors[selector]];
			event.count--;
			if (!event.count){
				styles.sheet.deleteRule(styles.sheet.cssRules.item(event.rule));
				keyframes.removeChild(event.keyframe);
				delete events[key];
				delete selectors[selector];
			}

			listeners.count--;
			listener.splice(index, 1);
			if (!listeners.count) startNames.forEach(function(name){
				this.removeEventListener(name, startEvent, false);
			}, this);
		}
	};

})();












// 'use strict';

// //// Jotter theme main js file



// // http://javascript.crockford.com/remedial.html
// if (!String.prototype.supplant) {
//   String.prototype.supplant = function (o) {
//     return this.replace(
//       /\{([^{}]*)\}/g,
//       function (a, b) {
//         var r = o[b];
//         return typeof r === 'string' || typeof r === 'number' ? r : a;
//       }
//     );
//   };
// }



// function jotterInit() {

//   // Contextual search
//   jsearchInit();

//   // Search suggestions

//   var ROW_TEMPLATE = '<span class="category {categoryClass}">{category}</span><span class="name">{term}</span>';
//   var ROW_TEMPLATE_USER = '<span class="category {categoryClass}">{category}</span><img src="{img}"><span class="name">{term}</span>';

//   $('#jsearch-form input, #jsearch-mobile-form input').autocomplete({

//     source: function(request, response) {

//       $.get('/api/suggest?q=' + request.term, function(suggestions) {
//         response(
//           suggestions.terms.map(function(term, i) {
//             return {
//               label: ROW_TEMPLATE.supplant({
//                 category: '&nbsp;',
//                 categoryClass: 'search',
//                 term: term
//               }),
//               value: term,
//               href: '/search/' + term
//             };
//           })

//           .concat(
//             suggestions.tags.map(function(tag, i) {
//               return {
//                 label: ROW_TEMPLATE.supplant({
//                   category: i ? '&nbsp;' : 'Topics',
//                   categoryClass: 'tags',
//                   term: tag,
//                 }),
//                 value: tag,
//                 href: '/tags/' + tag
//               };
//             })
//           )
//           .concat(
//             suggestions.users.map(function(user, i) {
//               return {
//                 label: ROW_TEMPLATE_USER.supplant({
//                   category: i ? '&nbsp;' : 'Users',
//                   categoryClass: 'users',
//                   img: user.picture,
//                   term: user.username,
//                 }),
//                 value: user.username,
//                 href: '/user/' + user.userslug
//               };
//             })
//           )
//         );
//       });
//     },

//     // Allow HTML in items, widget escapes tags by default, unescape them
//     open: function(event, ui) {
//       $("ul.ui-autocomplete li a").each(function() {
//         var htmlString = $(this).html();
//         htmlString = htmlString
//           .replace(/&lt;/g, '<')
//           .replace(/&gt;/g, '>')
//           .replace(/&amp;/g, '&');
//         $(this).html(htmlString);
//       });
//      },

//      select: function(event, ui) {
//        ajaxify.go(ui.item.href);
//      }
//   });

//   var suggestions = $('#search-form input').autocomplete('widget');
//   if (!suggestions.next().is('span.ui-arrow')) {
//     suggestions.after($('<span>').addClass('ui-arrow'));
//   }





//   // Content card, handle instant jot button click

//   var DEFAULT_ALERT_TIMEOUT = 3000;

//   $(document)
//     .off('click', '.content-card-save')
//     .on('click', '.content-card-save', function() {
//       var $this = $(this);
//       var isFavourite = $this.attr('data-favourited') === '1';
//       socket.emit(
//         isFavourite ? 'posts.unfavourite' : 'posts.favourite',
//         {
//           pid: $this.attr('data-pid'),
//           room_id: 'topic_' + $this.attr('data-tid')
//         },
//         function(err) {
//           var title = '<h4>' + $this.attr('data-title') + '</h4>';
//           if (err) {
//             app.alert({
//               message: title + err.message,
//               type: 'danger',
//               timeout: DEFAULT_ALERT_TIMEOUT
//             });
//           }
//           else {
//             $this.attr('data-favourited', isFavourite ? '0' : '1')
//             app.alert({
//               message: title + (isFavourite ? 'deleted' : ' saved'),
//               type: 'success',
//               timeout: DEFAULT_ALERT_TIMEOUT
//             });
//           }
//       });
//   });




//   // Content card, handle share buttons

//   function openShareFactory(url, params) {
//     return function(event) {
//       var $topicData = $(this).parent().find('.topic-card-data, .categories-card-data');
//       params.url = encodeURIComponent($topicData[0].href);
//       params.title = $topicData.attr('data-title');
//       window.open(
//         url.supplant(params),
//         '_blank',
//         'width=' + params.width + ',height=' + params.height + ',scrollbars=no,status=no'
//       );
//       return false;
//     };
//   }
//   $(document).on('click', '.instant-facebook-share',
//     openShareFactory('https://www.facebook.com/sharer/sharer.php?u={url}', {
//       width: 626,
//       height: 436
//     })
//   );
//   $(document).on('click', '.instant-twitter-share',
//     openShareFactory('https://twitter.com/intent/tweet?text={title}&url={url}', {
//       width: 550,
//       height: 420
//     })
//   );
//   $(document).on('click', '.instant-email-share',
//     openShareFactory('mailto:?Subject=Jotter Share: {title}&Body={url}', {
//       width: 550,
//       height: 420
//     })
//   );



//   // Clear all notifications on dropdown open
//   function notificationsClick() {
//     socket.emit('notifications.markAllRead', function(err) {
//       if (err) {
//         app.alertError(err.message);
//       }
//       $('.notification-icon')
//         .removeClass('unread-count')
//         .attr('data-content', 0);
//       Tinycon.setBubble(0);
//     });
//   }
//   $('.notifications.dropdown')
//     .off('click', notificationsClick)
//     .on('click', notificationsClick);





//   // Jot stuff
//   var $modalInitiator;
//   $(document)
//     .off('click', '.content-card-jot')
//     .on('click', '.content-card-jot', function() {
//       $('#jot-comment').val('');
//       $('#jot-widget--iframe').attr('src',
//         '/jot-widget?r=' + Math.random() + '&pid=' + $(this).attr('data-pid'));
//       $('#jot-widget-modal').attr('data-pid', $(this).attr('data-pid'));
//       $modalInitiator = $(this);
//     })
//     .off('click', '.jot-widget--jot')
//     .on('click', '.jot-widget--jot', function() {
//       var spaces = [].slice.call(
//         $('#jot-widget--iframe').contents().find('option:selected').map(function() {
//           return this.value;
//         })
//       );
//       var comment = $('#jot-comment').val();
//       var pid = $('#jot-widget-modal').attr('data-pid');
//       socket.emit(
//         'plugins.jotter.jots.jot',
//         {
//           pid: pid,
//           spaces: spaces,
//           note: comment
//         },
//         function(err, res) {
//           if (err) {
//             app.alert({
//               message: err.message,
//               type: 'danger',
//               timeout: DEFAULT_ALERT_TIMEOUT
//             });
//           }
//           if (res) {
//             app.alert({
//               message: '<h4>' + res[0].title + '</h4> jot\'ed to ' +
//                 res.map(function(item) {
//                   return item.space;
//                 }).join(', '),
//               type: 'success',
//               timeout: DEFAULT_ALERT_TIMEOUT
//             });
//           }
//         }
//       );
//       //$('#jot-widget-modal').modal('hide');
//       // Above is supposed to work, but, oh, Bootstrap
//       $modalInitiator.trigger('click');
//     });
// }

// /**
//  * Initialize contextual search.
//  *
//  * On submit:
//  *  -- if on tag page, search content for tag
//  *  -- if on jots page, search in user jots
//  *  -- if on search page, search all content
//  *  -- if on any other page, go to search page with specified term
//  *
//  *  Infinite scroll
//  *  To load more content after a search:
//  *
//  *  socket.emit('plugins.jotter.search.loadMore', {
//  *      after   : <num>,
//  *      query   : '<search-query>',
//  *      type    : 'tag' | 'jots | 'search',
//  *      context : '<tag-name>' | '<user-slug>' | '<search-query>'
//  *  })
//  */
// function jsearchInit() {

//   console.log('[plugins/jotter]', 'Jotter contextual search init.');
//   handleSearch();

//   var urlPatterns = {
//     //tag    : /^\/tags\/([^\/\?]+)/,
//     //jots   : /^\/user\/([^\/\?]+)\/jots/,
//     //jtopics : /^\/spaces/,
//     search  : /^\/search\/([^\/\?]+)/
//   };

//   function getContext () {

//     var context = {};

//     for (var k in urlPatterns) {
//       if (urlPatterns.hasOwnProperty(k)) {
//         var match = location.pathname.match(urlPatterns[k]);
//         if (match) {
//           context.type = k;
//           context.context = match[1];
//           return context;
//         }
//       }
//     }

//     context.type = 'search';
//     context.context = '';
//     return context;
//   }

//   $('#jsearch-form, #jsearch-mobile-form').off().on('submit', function (e) {
//     e.preventDefault();

//     var context = getContext();

//     var q = $('#jsearch-form input:visible, #jsearch-mobile-form input:visible').val().trim();


//     if (q) {
//       ajaxify.go('/search/' + encodeURIComponent(q));
//     }

//     return false;
//   });

//   function handleSearch() {
//     var searchButton = $("#search-button"),
//       searchFields = $("#search-fields"),
//       searchInput = $('#search-fields input');

//     $('#jsearch-form').off('submit').on('submit', dismissSearch);
//     searchInput.on('blur', dismissSearch);

//     function dismissSearch(e){
//       searchFields.hide();
//       searchButton.show();
//       e.preventDefault();
//     }

//     function prepareSearch() {
//       searchFields.removeClass('hide').show();
//       searchButton.hide();
//       searchInput.focus();
//     }

//     searchButton.on('click', function(e) {
//       prepareSearch();
//       return false;
//     });
//   }



//   // Workaround for Android devices
//   //$(window).off('touchmove').on('touchmove', function() {
//     //console.log('trigger scroll');
//     //$(window).trigger('scroll');
//   //});


//   // Workaround for Bootstrap modal
//   function preventDefault(e) {
//     //e.stopPropagation();
//     e.preventDefault();
//     //return false;
//   }
//   function disableScrolling() {
//     $('body').on('scroll touchmove mousewheel', preventDefault);
//   }
//   function enableScrolling() {
//     $('body').off('scroll touchmove mousewheel', preventDefault);
//   }
//   $(document)
//     .off('show.bs.modal', '*', disableScrolling)
//     .on( 'show.bs.modal', '*', disableScrolling)
//     .off('hide.bs.modal', '*', enableScrolling)
//     .on( 'hide.bs.modal', '*', enableScrolling);
// }


// // Handle normal page load and load via ajaxify

// //$(document).ready(jotterInit);
// $(window).on('action:ajaxify.contentLoaded', jotterInit);

