'use strict';

define(
  'forum/jinfinitescroll', [
    'forum/infinitescroll'
  ],
  function(infiniteScroll) {
    var $results, query, nextStart, template, fillingPage, container, loadingMore;

    function noop() {}

    var updateStateTimeout;

    function updateState() {
      if (updateStateTimeout) return;
      updateStateTimeout = setTimeout(function() {
        var state = history.state;
        state.top = $(window).scrollTop();
        history.replaceState(state, '');
        updateStateTimeout = null;
      }, 200);
    }

    function onPopState(e, container) {
      var state = e.originalEvent.state;
      $(window).one('action:ajaxify.end', function() {
        if (state.top) {
          $(container).html(state.html);
          $(window).scrollTop(state.top || 0);
        }
      });
    }

    function getAttrValue(attr) {
     return $('input[type="hidden"][template-variable="'+attr+'"]').val();
    }


    var containers = {
      'partials/users_list': {
        'id':'#people-container',
        'collection': 'users',
        'api': function(){
          return'/api/people';
        },
        'loadMoreSrc': function() {
          return query ?
            'plugins.jotter.search.loadMore' :
            'plugins.jotter.people.loadMore';
        },
        'loadQuery': function(after) {
          return {
            type:     'people',
            context:   '',
            query:     query,
            after:     nextStart,
            nextStart: nextStart
          };
        },
        'loadData': function(data) {
          return { users: data.users || data };
        }
      },

      'partials/communities/card': {
        'id':'#communities-container',
        'collection': 'categories',
        'api': function(){
          return'/api/communities';
        },
        'loadMoreSrc': function() {
          return query ?
            'plugins.jotter.search.loadMore' :
            'plugins.jotter.categories.loadMore';
        },
        'loadQuery': function(after) {
          return {
            type:     'categories',
            context:   '',
            query:     query,
            after:     nextStart,
            nextStart: nextStart
          };
        },
        'loadData': function(data) {
          return { categories: data.categories || data };
        }
      },

      'category': {
        'id':'#community-container',
        'collection': 'topics',
        'api': function(){
          return '/api/community/' + getAttrValue('communitySlug');
        },
        'loadMoreSrc': function() {
          return query ?
            'plugins.jotter.search.loadMore' :
            'categories.loadMore';
        },
        'loadQuery': function(after) {
            return  {
              type:    'topics',
              context: getAttrValue('communityId'),
              query:   query,
              after:   nextStart,
              cid:     getAttrValue('communityId')
            };
          },
          'loadData': function(data) {
            return  { topics: data.topics || data.categories};
          }
        },

        'partials/members_list': {
          'id':'#members-container',
          'collection': 'members',
          'loadMoreSrc': function() {
            return 'plugins.jotter.categories.members';
          },
          'loadQuery': function(after) {
            return  {
              after:   nextStart,
              cid:     getAttrValue('communityId')
            };
          },
          'loadData': function(data) {
            return  { members: data.members};
          }
        },

      };




    function parseAndTranslate(data, done) {
      infiniteScroll.parseAndTranslate(
        template,
        container.collection,
        container.loadData(data),
        function(html) {
          loadingMore = false;
          nextStart = typeof data.nextStart === 'number' ? data.nextStart : (nextStart + data.length);
          done();
          html.find('[title]').timeago();
          $results.append(html);
          // update state
          var state = history.state;
          state.nextStart = nextStart;
          state.html = $(container.id).html();
          history.replaceState(state, '');
          // load more if no vertical scroll
          if (!fillingPage && document.body.scrollHeight <= document.body.clientHeight) {
            fillingPage = true;
            loadMore();
          }
          else {
            fillingPage = false;
          }
        }
      );
    }


    function initSearch() {
      $results = $(container.id);
      query = '';
      nextStart = ajaxify.variables.get('nextStart');

      $('#contextual-search-form').off().on('submit', function(e) {
        e.preventDefault();
        query = $('#contextual-search-input').val().trim();
        nextStart = 0;
        $results.html('');
        if (query) {
          loadMore(template);
        }
        else {
          $.get(container.api(), function(data) {
            parseAndTranslate(data, noop);
            if ( typeof container.addFollowSeparator  === 'function'){
              container.addFollowSeparator();
            }
          });
        }
      });
    }

    function loadMore() {
      if (loadingMore) return;
      loadingMore = true;
      infiniteScroll.loadMore(
        container.loadMoreSrc(),
        container.loadQuery(),
        parseAndTranslate
      );
    }

    return {
      init: function(templatePage) {
        template = templatePage;
        container = containers[template];

        initSearch();

        if ( typeof container.addFollowSeparator  === 'function'){
          container.addFollowSeparator();
        }

        $(window)
          .off('scroll', updateState)
          .on('scroll', updateState)
          .off('popstate', function(event){ onPopState(event, container.id); })
          .on('popstate', function(event){ onPopState(event, container.id); });

        // var after = $(container.id).children().length;

        infiniteScroll.init(function(direction) {
           if (direction === 1) {
            loadMore();
          }
        });

        // load more if no vertical scroll
        if (!fillingPage && document.body.scrollHeight <= document.body.clientHeight) {
          fillingPage = true;
          loadMore();
        }
        else {
          fillingPage = false;
        }

      }

    };

  }
);

