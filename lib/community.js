'use strict';

/* globals define, ajaxify, app, utils, socket */


define('forum/community', function() {
  var Community = {}, communityId;

  Community.init = function() {
    initFollowing();
  };

  function initFollowing() {

    $('.member-button')
      .on('click', '#join-btn', function() {
        communityId = $('#join-btn').attr('data-cid');
        return toggleFollow('join');
      });
    
    $('.member-button')
      .on('click', '#leave-btn', function() {
        communityId = $('#leave-btn').attr('data-cid');
        return toggleFollow('leave');
      });
  }

  function toggleFollow(type) {
    socket.emit('plugins.jotter.categories.' + type, {
      cid: communityId
    }, function(err) {
      if (err) {
        return app.alertError(err.message);
      }
      
      $('#join-btn').toggleClass('hide', type === 'join');
      $('#leave-btn').toggleClass('hide', type === 'leave');
      var msg = type === 'join'? type + 'ed to ' : type + 'd ';
      app.alertSuccess('You have just been ' + msg + $('.content-navigation-item').html());
    });
    return false;
  }

  return Community;
});




