'use strict';

/* globals define, ajaxify, app, utils, socket */


define('forum/people', function() {
  var People = {}, theirid;

  People.init = function() {
    initFollowing();
    collapsePeopleList();
  };

  function collapsePeopleList () {
    $('#contextual-search-input').change(function() {
      var people_block = $('#collapse_people_list');
      $(this).val() ?  people_block .collapse('hide') : people_block .collapse('show');
    });
  }


  function initFollowing() {

    $('.users-container')
      .on('click', '.follow-btn', function() {
        var $this = $(this);
        theirid = $this.attr('data-uid');
        return toggleFollow($this, 'follow');
      });
    
    $('.users-container')
      .on('click', '.unfollow-btn', function() {
        var $this = $(this);
        theirid = $this.attr('data-uid');
        return toggleFollow($this, 'unfollow');
      });
  }

  function toggleFollow($item, type) {
    socket.emit('user.' + type, {
      uid: theirid
    }, function(err) {
      if (err) {
        return app.alertError(err.message);
      }
      var el = $item.parents('div[class^="user-information"]');
      el.find('.follow-btn').toggleClass('hide', type === 'follow');
      el.find('.unfollow-btn').toggleClass('hide', type === 'unfollow');
      app.alertSuccess('[[global:alert.' + type + ', ' + el.find('.account-username').html() + ']]');
    });
    return false;
  }


  return People;
});






